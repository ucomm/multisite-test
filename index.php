<?php get_header(); ?>

    <main role="main" aria-label="Content">
        <section>
            <h1><?php esc_html_e( 'Index', 'theme-boilerplate' ); ?></h1>
            <p>this is something....</p>
            <?php echo do_shortcode('[all_posts]'); ?>
        </section>
    </main>

<?php get_footer(); ?>