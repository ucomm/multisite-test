<?php
// Constants
define( 'THEME_BOILERPLATE_DIR', get_stylesheet_directory() );
define( 'THEME_BOILERPLATE_URL', get_stylesheet_directory_uri() );

// Load
require_once( 'vendor/autoload.php' );
require_once( 'lib/ScriptLoader.php' );
require_once('lib/ShortcodeHandler.php');

// Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', array('ThemeBoilerplate\Lib\ScriptLoader', 'load_scripts') );

add_shortcode('all_posts', 'ThemeBoilerplate\Lib\ShortcodeHandler::all_posts');