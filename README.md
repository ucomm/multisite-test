# <Multisite Test>

This is just for experimenting with ways of managing blog posts across different sites in a multisite context.


------

Please see our Work in Progress documentation via Basecamp on how to use these boilerplates: [Basecamp Docs](https://public.3.basecamp.com/p/c1jrAMzZsQtbDDzRjdYg6Ptd)

This repository serves as a boilerplate WordPress theme, that works well with composer, docker, and local build tools.

The theme files are strikingly simple, and are intended as a starting point for any theme.  It is not based on any parent theme such as Cornerstone or Beaver Builder theme.  In this repository, you will not find examples of things such as customizer options, menu additions, etc.  It is a very barebones starting point.

## Get Started

So you want to create a theme using the boilerplate.  In order to do so, you will need a few things installed:

- [Composer](https://getcomposer.org/).  This is the cornerstone to our package management.
- [NodeJS](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/lang/en/).  Required for build steps in packages that you install.
- [Docker](https://www.docker.com/).  Community edition is fine.  Virtualizes a local server and can run local build chains.

Apologies in advance for needing all of these on your machine.  The goal down the line is to have all of these requirements eliminated except for Docker.  Pull requests are welcome (:

If you have never worked with or created something from UComm's composer packages, you should first run:

```bash
composer config -g repositories.ucomm '{"type": "composer", "url": "https://packages.ucdev.net/"}'
```

The above will add a global reference to our packages hosted at UComm.  Almost all are privately accessed - meaning you must have a valid SSH key for our repositories on bitbucket in order to clone them.

You are not required to clone this project in any way to start theme development.  Instead, the following should be run at the inception of a new theme:

```bash
composer create-project ucomm/wp-theme-project-boilerplate <new-theme-name>
cd <new-theme-name>
composer install
composer run-script post-update-cmd
```

Next, you should run a search replace on the boilerplate directory to incorporate your theme name instead of boilerplate.

This will be an appropriate find/replace for the language namespace:

Find: `theme-boilerplate`

Replace: `<your-theme-slug>`

This will be an appropriate find/replace for the PHP namespace:

Find: `ThemeBoilerplate`

Replace: `<Your-Theme-Namespace>`

Next, remove the `*.lock` entry from .gitignore.  This was in place purely for development on this boilerplate, as a normal theme should have dependencies locked in.

Lastly, edit style.css to have the appropriate [WordPress theme metadata](https://developer.wordpress.org/themes/basics/main-stylesheet-style-css/#example).

To start development/see preview:

```bash
docker-compose up
open http://localhost
```

## Adding Plugins/Dependencies

This project uses composer as a dependency management tool for PHP.  It also uses NPM/Yarn as a dependency management tool for local build tools, to compile/minify CSS/JS.

Packages can be found:

- [Composer Packagist](https://packagist.org/)
- [WordPress Packagist](https://wpackagist.org/)
- [UConn Communications Packagist](https://packages.ucdev.net/)
- [NPM Packages (JavaScript)](https://www.npmjs.com/)

## Branching Strategy

We try to use [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) as our branching strategy.  Not required, but helpful when working on a team.

Tags must follow the [semver system](http://semver.org/).

## Debugging

You can access Docker containers directly from the shell (as long as they are running), example:

```bash
docker-compose exec web bash
```