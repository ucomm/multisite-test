<?php get_header(); 



?>

    <main role="main" aria-label="Content">
        <section>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php the_content(); ?>
                    <?php edit_post_link(); ?>
                </article>
            <?php endwhile; ?>

            <?php else : ?>
                <?php get_template_part('template-parts/content', 'none'); ?>
            <?php endif; ?>
        </section>
    </main>

<?php get_footer(); ?>