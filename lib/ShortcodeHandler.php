<?php

namespace ThemeBoilerplate\Lib;

class ShortcodeHandler {
  static private function count_posts($sites) {
    $total = 0;
    foreach ($sites as $site) {
      switch_to_blog($site->blog_id);
      $posts = wp_count_posts();
      $total += $posts->publish;
      restore_current_blog();
    }
    return $total;
  }
  static private function query_sites($sites) {
    $all_posts = array();
    foreach ($sites as $site) {
      // switch to each one and query it
      switch_to_blog($site->blog_id);
      $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => -1
      );
      $query = new \WP_Query($args);
      
      if ($query->have_posts()) {
        while ($query->have_posts()) {
          $site_name = get_bloginfo('name');
          $query->the_post();
          $the_post = get_post();
          $post_id = $the_post->ID;
          $post_date = $the_post->post_date;
          $post_excerpt = $the_post->post_excerpt;
          $permalink = get_permalink($post_id);
          $post_title = $the_post->post_title;

          $post_data = (object)array(
            'post_excerpt' => $post_excerpt,
            'post_date' => $post_date,
            'permalink' => $permalink,
            'site_name' => $site_name,
            'post_title' => $post_title
          );

          // push the results of the query to the all_posts array
          array_push($all_posts, $post_data);
        }
      }
      // switch out of the blog to go to the next.
      restore_current_blog();
    }

    // sort the posts by date
    usort($all_posts, array(__CLASS__, 'sort_posts_by_date'));
    return $all_posts;
  }
  static private function sort_posts_by_date($a, $b) {
    $a_date = strtotime($a->post_date);
    $b_date = strtotime($b->post_date);
    if ($b_date === $a_date) {
      return 0;
    } else {
      return $b_date - $a_date;
    }
  }
  static public function all_posts($atts, $content = null) {
    $a = shortcode_atts(array(
      'posts_per' => -1
    ), $atts);

    $all_posts = null;
    $sites = get_sites();
    $post_count = self::count_posts($sites);
    $posts_per = (int)$a['posts_per'];
    $post_total = $post_count + $posts_per;
    $transient_id = "uc_blogs_" . md5($post_total);

    if (!get_transient($transient_id)) {
      $all_posts = self::query_sites($sites);
      set_transient($transient_id, $all_posts, DAY_IN_SECONDS);
    } else {
      $all_posts = get_transient($transient_id);
    }

    if ($posts_per > 0) {
      $all_posts = array_slice($all_posts, 0, $posts_per);
    }

    // begin output buffering for the shortcode.
    $output = ob_start();

    require(THEME_BOILERPLATE_DIR . '/template-parts/post-details.php');

    // end output buffering and return.
    $output = ob_get_clean();
    return $output;
  }
}