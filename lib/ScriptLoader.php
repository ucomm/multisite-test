<?php

namespace ThemeBoilerplate\Lib;

class ScriptLoader {

    public static function load_scripts() {
        wp_enqueue_style('theme-boilerplate-styles', get_stylesheet_directory_uri() . '/build/main.css');
        wp_enqueue_style('uconn-banner', get_stylesheet_directory_uri() . '/vendor/uconn/banner/_site/banner.css');

        wp_enqueue_script('theme-boilerplate-script', get_stylesheet_directory_uri() . '/build/index.js', array('jquery'), false, true);
    }

}