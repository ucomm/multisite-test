<?php // This loop is intended for locations where there are lists of posts.  Not singles or pages. ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <h2>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
        </h2>

        <?php the_excerpt(); ?>

        <?php edit_post_link(); ?>
    </article>
<?php endwhile; ?>

<?php else : ?>
    <?php get_template_part('template-parts/content', 'none'); ?>
<?php endif; ?>