<ul>
<?php

foreach ($all_posts as $post) {
?>
  <li>
    <p><?php echo $post->site_name; ?></p>
    <a href="<?php echo $post->permalink; ?>">
      <h2>
        <?php echo $post->post_title; ?>
      </h2>
    </a>
    <p><?php $post->post_excerpt; ?></p>
    <p><?php echo $post->post_date; ?></p>
  </li>
<?php
}

?>
</ul>



